import React, {Component} from 'react';
import './App.css';
// import Wrapper from "./hoc/Wrapper";
import Modal from "./components/UI/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";


class App extends Component {
    state = {
        modalShow: false,
        alerts: [
            {type: 'warning', id: 1},
            {type: 'success', id: 2},
            {type: 'primary', id: 3},
            {type: 'danger', id: 4}
        ]
    };

    showModalDialog = () => this.setState({modalShow: true});

    closeModalDialog = () => this.setState({modalShow: false});

    dismissHandler = (id) => {
        const index = this.state.alerts.findIndex(alert => alert.id === id);
        const alerts = [...this.state.alerts];
        alerts.splice(index, 1);
        this.setState({alerts});
    }


    render() {
        return (
            <div style={{width: '50%', margin: '0 auto'}}>
                <button onClick={this.showModalDialog}>Show Modal</button>
                <Modal
                    show={this.state.modalShow}
                    close={this.closeModalDialog}
                    title="Some kinda modal title"
                >
                    <p>This is modal content</p>
                </Modal>

                {this.state.alerts.map(alert =>
                    <Alert
                        type={alert.type}
                        dismiss={() => this.dismissHandler(alert.id)}
                        key={alert.id}>
                        This is a {alert.type} type alert
                    </Alert>
                )}
                
                <Alert>Just some alert without type</Alert>

            </div>
        );
    }
}

export default App;
