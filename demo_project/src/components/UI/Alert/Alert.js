import React from 'react';
import './Alert.css';

const Alert = props => {
    const button = (<button className="closeAlert" onClick={props.dismiss}>
                        <i className="fas fa-times"/>
                    </button>);
    const type = props.type ? props.type : 'primary';

    return (
        <div className={['Alert', type].join(' ')}>
            <p>{props.children}</p>
            {props.dismiss ? button : null}
        </div>
    )
};

export default Alert;