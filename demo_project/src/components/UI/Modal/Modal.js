import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";
import Wrapper from "../../../hoc/Wrapper";

// const btns = [
//     {type: 'primary', label: 'Continue', clicked: this.continued},
//     {type: 'danger', label: 'Close', clicked: this.closed}
// ].map(btn => <Btn class={btn.type}>)

const Modal = props => (
    <Wrapper>
        <Backdrop show={props.show} clicked={props.close}/>
        <div
            className="Modal"
            style={{
                transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
        >
            <h1>{props.title}</h1>
            <button onClick={props.close} className='closeModalBtn'><i className="fas fa-times"/></button>
            <hr/>
            {props.children}

        </div>
    </Wrapper>
);

export default Modal;